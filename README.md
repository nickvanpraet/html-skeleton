## Skeleton Docs


### Scaffolding files for new env creation
Place files inside etc/scaffold/templates nested as if etc/scaffold/templates was the project root.

Example:

You want to make sure that all new environments, when deployed, have a settings file in docroot/src/settings.yml
Simply create a settings.yml file at etc/scaffold/templates/docroot/src/settings.yml

During new environment creation this will then create etc/scaffold/<environment>/docroot/src/settings.yml

### Scaffolding files for a specific environment
Place files inside etc/scaffold/<environment> nested as if etc/scaffold/<environment> was the project root.

Example:

etc/scaffold/staging/docroot/src/settings.yml exists, created when staging was created.
During deploys, this file will be copied to docroot/src/settings.yml.
Additionally, during this copy step, certain tokens will also be replaced by their corresponding values.

Given this as the contents of that .yml file:

environment: [[ environment_name ]]
project: [[ project_name ]]
something: else

After a deploy to staging, the settings.yml file inside docroot/src will contain, for example:

environment: staging
project: fancyboyproject1
something: else

Soon(tm) these tokens will also support database credentials.

